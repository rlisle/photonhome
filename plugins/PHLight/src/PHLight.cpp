/******************************************************************
 PHLight LED dimming control

 Features:
 - On/Off control
 - Smooth dimming with duration

 Supported Attributes:
 - Switch
 - Brightness
 - Transition

 http://www.github.com/rlisle/PhotonHome

 Written by Ron Lisle

 BSD license, check license.txt for more information.
 All text above must be included in any redistribution.

 Datasheets:

 Changelog:
 2019-07-29: Add local pin support. Move optional init parameters to functions.
 2019-05-15: Initial version refactored from Patriot
 ******************************************************************/

#include "PHLight.h"

#define kDebounceDelay 50

// PUBLIC FUNCTIONS

/**
 * Constructor
 * @param pinNum is the pin number that is connected to the light.
 * @param name String name used to address the light.
 * @param isInverted True if On = output LOW
 * @param forceDigital True if output On/Off only (even if pin supports PWM)
 */
PHLight::PHLight(int pinNum, String name)
        : Device(name, "light"),
          _pin(pinNum)
{
    _isInverted               = false;
    _forceDigital             = false;

    _localPinNum              = 0;          // 0 = none
    _localPinName             = "unnamed";
    _localPinActiveHigh       = false;
    _lastReadTime             = 0;

    _dimmingPercent           = 100;                            // Default: 100%
    _dimmingDuration          = isPwmSupported() ? 2.0 : 0;     // Default: 2 seconds
    _currentPercent           = 0.0;        // May want to leave this retained
    _targetPercent            = 0;          // May want to leave this retained
    _incrementPerMillisecond  = 0.0;
    _lastUpdateTime           = 0;

    pinMode(pinNum, OUTPUT);
    outputPWM();                        // Set initial state
}

/**
 * setAttribute
 * 
 * @param attribute (lowercase "switch", "brightness", or "transition")
 * @param value String numerical value
 */
void PHLight::setAttribute(String attribute, String value) {
    if(attribute == "switch") {
        setSwitch(value);
    } else if(attribute == "brightness") {
        setBrightness(value);        
    } else if(attribute == "transition") {
        setTransition(value);
    }
}

/**
 * queryAttribute - return the current value for an attribute
 * 
 * @param attribute (lowercase "switch" or "brightness")
 */
String PHLight::queryAttribute(String attribute) {
    if(attribute == "switch") {
        return getSwitch();
    } else if(attribute == "brightness") {
        return getBrightness();
    } else if(attribute == "transition") {
        return getTransition();
    }
    return "";
}

/*
 * Get Configuration
 * Return Home Assistant configuration for MQTT autodiscovery
 * @param controllerName String 
 * @return String Home Assistant configuration JSON
 */
String PHLight::getConfiguration(String controllerName) {
    String config = "{\"name\":\"" + deviceID() + "\","
                + "\"cmd_t\":\"" + controllerName + "/" + deviceID() + "/switch/set\""
                + ",\"bri_cmd_t\":\"" + controllerName + "/" + deviceID() + "/brightness/set\""
                + ",\"retain\":true"
                + "}";
    return config;
}

void PHLight::setLocalPin(int pinNum, String pinName, bool activeHigh) {
    _localPinNum = pinNum;
    _localPinName = pinName;
    _localPinActiveHigh = activeHigh;
    pinMode(pinNum, INPUT_PULLUP);
}

void PHLight::setInverted(bool isInverted) {
    _isInverted = isInverted;
}

void PHLight::setDigital(bool isDigital) {
    _forceDigital = isDigital;
}

// PRIVATE FUNCTIONS

int PHLight::stringToPercent(String value) {
    int percent = value.toInt();
    if(percent < 0) percent = 0;
    if(percent > 100) percent = 100;
    return percent;
}

String PHLight::percentToString(int value) {
    return String(value);
}

/**
 * Set switch
 * @param value String "0" to "100"
 */
void PHLight::setSwitch(String value) {
    if(value == "on") {
        changePercent(_dimmingPercent);
    } else if(value == "off") {
        changePercent(0);
    }
}

/**
 * Get switch
 * @return String current 0-100 percent value
 */
String PHLight::getSwitch() {
    return _targetPercent == 0 ? "off" : "on";
}

/**
 * Set brightness
 * @param value String "0" to "100"
 */
void PHLight::setBrightness(String value) {
    int percent = stringToPercent(value);
    _dimmingPercent = percent;
    changePercent(percent);
}

/**
 * Get brightness
 * @return String current 0-100 percent value
 */
String PHLight::getBrightness() {
    return percentToString(_currentPercent);
}

/**
 * Set transition
 * @param String milliseconds "0" to "30000"
 */
void PHLight::setTransition(String value) {
    _dimmingDuration = value.toFloat();
}

/**
 * Get transition
 * @return String milliseconds
 */
String PHLight::getTransition() {
    return String(_dimmingDuration);
}

/**
 * Change percent
 * @param percent Int new percent value
 */
void PHLight::changePercent(int percent) {
    if(_targetPercent == percent) return;

    _targetPercent = percent;
    if(_dimmingDuration == 0.0 || isPwmSupported() == false) {
        _currentPercent = percent;
        outputPWM();

    } else {
        startSmoothDimming();
    }
}

/**
 * Start smooth dimming
 */
void PHLight::startSmoothDimming() {
    if((int)_currentPercent != _targetPercent){
        _lastUpdateTime = millis();
        float delta = _targetPercent - _currentPercent;
        _incrementPerMillisecond = delta / (_dimmingDuration * 1000);
    }
}

/**
 * loop()
 */
void PHLight::loop()
{
    // Poll switch
    if (isTimeToCheckSwitch())
    {
        if (didSwitchChange())
        {
            //TODO: Set light on/off as a result of using switch
            //      This overrides stuck commands, etc.
            //      May want to provide an optional override and/or dimming later
            if(_switchState == _localPinActiveHigh) {   // Is turning ON?
                changePercent(100);
            } else {
                changePercent(0);
            }
        }
    }

    // Is fading transition underway?
    if(_currentPercent == _targetPercent) {
        // Nothing to do.
        return;
    }

    long loopTime = millis();
    float millisSinceLastUpdate = (loopTime - _lastUpdateTime);
    _currentPercent += _incrementPerMillisecond * millisSinceLastUpdate;
    if(_incrementPerMillisecond > 0) {
        if(_currentPercent > _targetPercent) {
            _currentPercent = _targetPercent;
        }
    } else {
        if(_currentPercent < _targetPercent) {
            _currentPercent = _targetPercent;
        }
    }
    _lastUpdateTime = loopTime;
    outputPWM();
};

/**
 * isTimeToCheckSwitch()
 * @return bool if enough time has elapsed to sample switch again
 */
bool PHLight::isTimeToCheckSwitch()
{
    long currentTime = millis();
    if (currentTime < _lastReadTime + kDebounceDelay)
    {
        return false;
    }
    _lastReadTime = currentTime;
    return true;
}

/**
 * didSwitchChange()
 * @return bool if switch has changed since last reading
 */
bool PHLight::didSwitchChange()
{
    bool newState = digitalRead(_localPinNum) == 0;
    if (newState == _switchState)
    {
        return false;
    }
    _switchState = newState;
    return true;
}

/**
 * Set the output PWM value (0-255) based on 0-100 percent value
 */
void PHLight::outputPWM() {
    if(isPwmSupported()) {
        float pwm = _currentPercent;
        pwm *= 255.0;
        pwm /= 100.0;
        analogWrite(_pin, (int) pwm);
    } else {
        bool isOn = _currentPercent > 49;
        bool isHigh = (isOn && !_isInverted) || (!isOn && _isInverted);
        digitalWrite(_pin, isHigh ? HIGH : LOW);
    }
}

/**
 * Is PWM pin?
 * @param pin number
 * @return bool true if pin supports PWM
 */
bool PHLight::isPwmSupported()
{
    switch(_pin) {
        case D0:
        case D1:
        case D2:
        case D3:
        case A4:
        case A5:
        case A7:    // aka WKP
        case RX:
        case TX:
            return _forceDigital ? FALSE : TRUE;
        default:
            break;
    };
    return FALSE;
}